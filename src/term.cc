#include "term.h"

Term::Term() : Window(InitScreen())
{
}

Term::~Term()
{
    ::endwin();
}

WINDOW *Term::InitScreen()
{
    ::initscr();
    return stdscr;
}

void Term::Init()
{
    ::keypad(stdscr, TRUE);
    SetEcho(false);
    // Disable cursor   --   cursor::set_visibility(cursor::visibility::invisible);

    if (!::has_colors())
    {
        Write("Your terminal does not support color");
    }
    else
    {
        InitColor();
    }
    NoDelay(true);
}

void Term::InitColor()
{
    ::start_color();
    ::use_default_colors();
    ::init_pair(1, COLOR_WHITE, COLOR_BLACK);
    ::init_pair(2, COLOR_BLACK, COLOR_RED);
    ::init_pair(3, COLOR_WHITE, COLOR_BLUE);
    ::init_pair(4, COLOR_BLACK, COLOR_YELLOW);
    ::init_pair(5, COLOR_BLUE, -1);
    ::init_pair(6, COLOR_MAGENTA, -1);
    ::init_pair(7, COLOR_BLUE, COLOR_GREEN);
    ::init_pair(8, COLOR_GREEN, -1);
    ::init_pair(9, COLOR_YELLOW, -1);
    ::init_pair(10, COLOR_RED, -1);
    ::init_pair(11, COLOR_BLUE, COLOR_YELLOW);
    ::init_pair(12, COLOR_BLUE, COLOR_RED);
    ::init_pair(13, COLOR_BLUE, COLOR_GREEN);
}

void Window::SetColor(int color)
{
    ::wattron(thisWindow.get(), COLOR_PAIR(color));
}

void Window::UnsetColor(int color)
{
    ::wattroff(thisWindow.get(), COLOR_PAIR(color));
}

int Term::SetEcho(bool echo)
{
    if (echo)
    {
        return ::echo();
    }

    return ::noecho();
}

Window Term::NewWindow(int x1, int y1, int x2, int y2)
{
    return Window(x1, y1, x2, y2);
}

coord Window::Size() const
{
    coord c;
    getmaxyx(thisWindow.get(), c.y, c.x);
    return c;
}

void Window::Refresh()
{
    ::wrefresh(thisWindow.get());
}

void Window::Border()
{
    ::wborder(thisWindow.get(), 0, 0, 0, 0, 0, 0, 0, 0);
}

void Window::Erase()
{
    ::werase(thisWindow.get());
}

void Window::FillBg(chtype c)
{
    ::wbkgd(thisWindow.get(), c);
}

int Window::GetKey()
{
    return wgetch(thisWindow.get());
}

void Window::NoDelay(bool d)
{
    ::nodelay(thisWindow.get(), d);
}

void Window::WriteXY(int x, int y, const std::string &str) const
{
    mvwaddstr(thisWindow.get(), y, x, str.c_str());
}

void Window::WriteChar(chtype c)
{
    ::waddch(thisWindow.get(), c);
}

void Window::Write(const std::string &str) const
{
    waddstr(thisWindow.get(), str.c_str());
}
void ColorBar(Window &w, const std::string &barStr, float percent, int color1, int color2) {
    size_t percentPos = (long int)((float)barStr.size() * (percent / 100));
    w.SetColor(color1);
    w.Write(barStr.substr(0, percentPos));
    w.SetColor(color2);
    w.Write(barStr.substr(percentPos));
}
