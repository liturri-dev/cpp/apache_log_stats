#ifndef APP_H
#define APP_H
#include "config.h"
#include "log_stats.h"
#include "os_info.h"
#include "term.h"
#include "tools.h"
#include <string>
#include <vector>

typedef std::pair<std::string, PathSummary> PathPair;

class StatApp
{
public:
    StatApp(const std::string &filename);
    void Run();
    void RefreshScreen();

    void DisplayTopTen(Window &w, const std::vector<PathPair> &topItemsList);
    void DisplayStats(Window &w);

    void ShowHelp();
    // void ShowOtherStats();

private:
    Term t;
    ParseLogFile parser;
    bool keepRunning = true;
    int displayMode = 1;
    int sortColumn = 2;
    OSInfo osInfo;
    // bool showOtherStats = false;
};

#endif // APP_H
