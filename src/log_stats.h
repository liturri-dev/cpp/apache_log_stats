#ifndef __LOG_STATS_H__
#define __LOG_STATS_H__
#include "config.h"
#include "follow.h"
#include "logline.h"
#include "summaries.h"
#include <chrono>
#include <fstream>
#include <list>
#include <map>
#include <unordered_map>
#include <string>

struct LoadCounters
{
    int counter1=0, counter5=0, counter15=0;
    float rate1=0, rate5=0, rate15=0;
    time_t minTime;
};

class TimeCounters
{
private:
    std::unordered_map<time_t, unsigned int> countersList;

public:
    void clear() { countersList.clear(); }
    unsigned int &operator[](const time_t &key) { return countersList[key]; };
    void TimeCleanup();
    const LoadCounters GetLoadCounters() const;
};

typedef std::unordered_map<std::string, PathSummary> PathSummaryMap;
typedef std::unordered_map<std::string, SourceSummary> SourceSummaryMap;

class ParseLogFile
{
private:
    std::list<LogLine> urlList;
    PathSummaryMap counterPerPath;
    SourceSummaryMap counterPerSource;
    TimeCounters timeCounters;
    // time passed since the last clean up of the counters
    std::chrono::time_point<std::chrono::steady_clock> lastCleanUp = std::chrono::steady_clock::now();

    Follow logFile;

    long linesParsed = 0;

    // int itemsKeepTime;
    // int refreshTime;

protected:
    void CleanOldEntries();

public:
    ParseLogFile(const std::string &filename) : logFile(filename, 80000){};
    void Loop();
    const PathSummaryMap GetTopTenPath() const;
    const SourceSummaryMap GetTopTenSource() const;

    // const TimeCounters &GetTimeCounters() const { return timeCounters; }
    const LoadCounters GetLoadCounters() const { return timeCounters.GetLoadCounters(); };
    const auto &GetUrlList() const { return urlList; };
    void RestartCounters();
};

#endif