#ifndef TOOLS_H
#define TOOLS_H

#include <fmt/core.h>
#include <sstream>
#include <string>

std::string Float2Units(double n, int decimals = 1, bool base10 = false);

// template returns position integer from value between ranges
template <typename T, typename... T2>
int ramp(T value, T2... args)
{
    int pos = 0;
    int i = 0;
    int n = sizeof...(args);
    T arr[] = {args...};
    while (i < n)
    {
        if (value < arr[i])
            return pos;
        pos++;
        i++;
    };
    return pos;
}

#endif