#ifndef OS_INFO_H
#define OS_INFO_H

#include "tools.h"
#include <chrono>
#include <cstdint>
#include <sstream>
#include <string>
#include <unistd.h>
#include <vector>

struct CPULoad
{
    float load1;
    float load5;
    float load15;
};

struct MemInfo
{
    uint64_t totalMem = 0;
    uint64_t freeMem = 0;
    uint64_t usedMem = 0;
    uint64_t cacheMem = 0;
    uint64_t swapTotal = 0;
    uint64_t swapFree = 0;
    uint64_t swapUsed = 0;
    uint64_t availableMem = 0;

    float perc_freeMem = 0;
    float perc_usedMem = 0;
    float perc_cacheMem = 0;
    float perc_swapUsed = 0;
    float perc_swapFree = 0;
    float perc_availableMem = 0;
    std::string values();
    std::string percents();
};

struct ProcInfo
{
    size_t pid;
    size_t ppid;
    std::string command;
};

typedef std::vector<std::pair<std::string, int>> ProcListPair;

class OSInfo
{
private:
    long pageSize;
    int cpuCount;
    CPULoad cpuLoad;
    MemInfo memInfo;

    std::vector<ProcInfo> procList;
    ProcListPair orderedProcList;
    std::chrono::time_point<std::chrono::steady_clock> updateTime;

protected:
    void RefreshLoadAvg();
    void RefreshMemInfo();
    void RefreshProcessList();


public:
    OSInfo();
    void Refresh();
    int GetCpuCount() const { return cpuCount; };
    size_t GetProcessCount() const { return procList.size(); };
    const ProcListPair GetOrderedProcessList() const { return orderedProcList; };

    const CPULoad GetCpuLoad() const { return cpuLoad; }
    const MemInfo GetMemInfo() const { return memInfo; }
};

#endif // OS_INFO_H
