#include "app.h"
#include <cmath>
#include <fmt/core.h>
#include <functional>
#include <utility>
#include <vector>

StatApp::StatApp(const std::string &filename) : t(), parser(filename)
{
    setlocale(LC_ALL, "");
    t.Init();
}

void StatApp::Run()
{
    time_t timer = time(NULL) - 4;

    while (keepRunning)
    {
        parser.Loop();
        auto ch = t.GetKey();
        // if (ch == ERR)
        //     return;
        switch (ch)
        {
        case 'q':
            keepRunning = false;
            break;
        case ' ':
        case KEY_RESIZE:
            timer = 0;
            break;
        case 'd':
            displayMode = (displayMode + 1) % 2;
            timer = 0;
            break;
        case 's':
            // Next column sort
            sortColumn = (sortColumn + 1) % 5;
            timer = 0;
            break;
        case 'S':
            // Previous column sort
            sortColumn = (sortColumn + 4) % 5;
            timer = 0;
            break;
        case '?':
        case 'h':
            // Show help
            ShowHelp();
            timer = 0;
            break;
        case 'r':
            // Restart counters
            parser.RestartCounters();
            timer = 0;
            continue;
            // case 'o':
            //     // Show app stats
            //     showOtherStats = true;
            //     timer = 0;
            //     break;
        }
        if (time(NULL) - timer > cfg::GetRefreshTime())
        {
            timer = time(NULL);
            RefreshScreen();
        }
    };
}

void StatApp::RefreshScreen()
{
    std::vector<PathPair> topItemsList;
    std::function<bool(const PathPair &, const PathPair &)> sortCmp;
    switch (sortColumn)
    {
    case 0:
        sortCmp = [](const PathPair &a, const PathPair &b)
        { return a.second.GetCount() > b.second.GetCount(); };
        break;
    case 1:
        sortCmp = [](const PathPair &a, const PathPair &b)
        { return a.second.GetCountPerSec() > b.second.GetCountPerSec(); };
        break;
    case 2:
        sortCmp = [](const PathPair &a, const PathPair &b)
        { return a.second.GetSumTime() > b.second.GetSumTime(); };
        break;
    case 3:
        sortCmp = [](const PathPair &a, const PathPair &b)
        { return a.second.GetSumTime() / static_cast<float>(a.second.GetCount()) > b.second.GetSumTime() / static_cast<float>(b.second.GetCount()); };
        break;
    case 4:
        sortCmp = [](const PathPair &a, const PathPair &b)
        { return a.second.GetNormalizeKey() > b.second.GetNormalizeKey(); };
        break;
    }
    // sortCmp = [](const PathPair &a, const PathPair &b)
    // { return a.second.GetSumTime() > b.second.GetSumTime(); };
    switch (displayMode)
    {
    case 0:
        for (auto &item : parser.GetTopTenSource())
            topItemsList.push_back(std::make_pair(item.first, item.second));
        break;
    case 1:
        for (auto &item : parser.GetTopTenPath())
            topItemsList.push_back(std::make_pair(item.first, item.second));
        break;
    }
    std::sort(topItemsList.begin(), topItemsList.end(), sortCmp);
    auto wStats = t.NewWindow(1, 0, t.Size().x - 1, 5);
    DisplayStats(wStats);
    auto wTopTen = t.NewWindow(1, 4, t.Size().x - 1, t.Size().y - 1);
    DisplayTopTen(wTopTen, topItemsList);
    // t.Refresh();
    // if (showOtherStats)
    //     ShowOtherStats();
}

void StatApp::DisplayTopTen(Window &w, const std::vector<PathPair> &topItemsList)
{
    int count = 0;
    int col = 0;
    int winMaxX = w.Size().x;
    w.SetColor(1);
    w.Erase();
    // w.FillBg();
    // w.Border();
    // ::box(stdscr, 0, 0);
    // auto &win = t.GetWin();
    w.SetColor(col++ == sortColumn ? 4 : 3);
    w.WriteXY(0, 0, fmt::format("{:>7}", "Count"));
    w.SetColor(col++ == sortColumn ? 4 : 3);
    w.Write(fmt::format("{:>8}", "Count"));
    w.SetColor(col++ == sortColumn ? 4 : 3);
    w.Write(fmt::format("{:>10}", "Time"));
    w.SetColor(col++ == sortColumn ? 4 : 3);
    w.Write(fmt::format("{:>10}", "Time"));
    w.SetColor(col++ == sortColumn ? 4 : 3);
    w.Write(fmt::format("{:^{}}", "", winMaxX - 37));
    col = 0;
    w.SetColor(col++ == sortColumn ? 4 : 3);
    w.WriteXY(0, 1, fmt::format("{:>7}", "Total"));
    w.SetColor(col++ == sortColumn ? 4 : 3);
    w.Write(fmt::format("{:>8}", "PerSec"));
    w.SetColor(col++ == sortColumn ? 4 : 3);
    w.Write(fmt::format("{:>10}", "Total"));
    w.SetColor(col++ == sortColumn ? 4 : 3);
    w.Write(fmt::format("{:>10}", "P/Call"));
    w.SetColor(col++ == sortColumn ? 4 : 3);
    w.Write(fmt::format("{:^{}}", "URL", winMaxX - 37));
    w.SetColor(5);
    for (auto item : topItemsList)
    {
        if (count > t.Size().y - 7)
            break;
        count++;
        w.WriteXY(0, count + 1,
                  fmt::format("{:7d}{:8.2f}{:10.2f}{:10.2f} {:<{}}", item.second.GetCount(), item.second.GetCountPerSec(),
                              item.second.GetSumTime(), item.second.GetSumTime() / static_cast<float>(item.second.GetCount()), item.second.GetMaxLenKey(winMaxX - 39), winMaxX - 39));
    }
    w.Refresh();
}

void StatApp::DisplayStats(Window &w)
{
    auto c = parser.GetLoadCounters();
    w.SetColor(6);
    w.WriteXY(0, 0, "Reqs ");
    w.SetColor((c.minTime < static_cast<time_t>(1 * 60)) ? 5 : 8);
    w.Write(fmt::format("{:>6.2f} ", c.rate1));
    w.SetColor((c.minTime < static_cast<time_t>(5 * 60)) ? 5 : 8);
    w.Write(fmt::format("{:>6.2f} ", c.rate5));
    w.SetColor((c.minTime < static_cast<time_t>(15 * 60)) ? 5 : 8);
    w.Write(fmt::format("{:>6.2f}  ", c.rate15));

    osInfo.Refresh();

    auto cpuLoad = osInfo.GetCpuLoad();
    auto mem = osInfo.GetMemInfo();
    int cpuCount = osInfo.GetCpuCount();
    const auto orderList = osInfo.GetOrderedProcessList();

    w.SetColor(6);
    w.WriteXY(28, 0, "PS/Tot:");
    w.SetColor(osInfo.GetProcessCount() > 1200 ? 5 : 8);
    w.Write(fmt::format("{:>4d} ", osInfo.GetProcessCount()));
    int procIndex = 0;
    w.SetColor(6);
    w.Write(" " + orderList[procIndex].first + ":");
    w.SetColor(orderList[procIndex].second > 20 ? 5 : 8);
    w.Write(fmt::format("{:>4d} ", orderList[procIndex++].second));
    w.SetColor(6);
    w.Write(" " + orderList[procIndex].first + ":");
    w.SetColor(orderList[procIndex].second > 20 ? 5 : 8);
    w.Write(fmt::format("{:>4d} ", orderList[procIndex++].second));
    w.SetColor(6);
    w.Write(" " + orderList[procIndex].first + ":");
    w.SetColor(orderList[procIndex].second > 20 ? 5 : 8);
    w.Write(fmt::format("{:>4d} ", orderList[procIndex++].second));
    w.SetColor(6);
    w.Write(" " + orderList[procIndex].first + ":");
    w.SetColor(orderList[procIndex].second > 20 ? 5 : 8);
    w.Write(fmt::format("{:>4d} ", orderList[procIndex++].second));
    w.SetColor(6);
    w.Write(" " + orderList[procIndex].first + ":");
    w.SetColor(orderList[procIndex].second > 20 ? 5 : 8);
    w.Write(fmt::format("{:>4d} ", orderList[procIndex++].second));
    // w.SetColor(6);
    // w.Write(" apache:");
    // w.SetColor(proces.apache > 700 ? 5 : 8);
    // w.Write(fmt::format("{:>4d} ", proces.apache));
    // w.SetColor(6);
    // w.Write(" php:");
    // w.SetColor(proces.php > 100 ? 5 : 8);
    // w.Write(fmt::format("{:>4d} ", proces.php));

    // w.SetColor(6);
    w.WriteXY(0, 1, "Load ");
    w.SetColor(ramp(cpuLoad.load1, (float)cpuCount / 2.0f, (float)cpuCount) + 8);
    w.Write(fmt::format("{:>6.2f} ", cpuLoad.load1));
    w.SetColor(ramp(cpuLoad.load5, (float)cpuCount / 2.0f, (float)cpuCount) + 8);
    w.Write(fmt::format("{:>6.2f} ", cpuLoad.load5));
    w.SetColor(ramp(cpuLoad.load15, (float)cpuCount / 2.0f, (float)cpuCount) + 8);
    w.Write(fmt::format("{:>6.2f}  ", cpuLoad.load15));
    w.SetColor(6);
    auto perc = 100 - mem.perc_availableMem;
    auto barText = fmt::format("{}/{}(%{:.2f})", Float2Units((float)mem.usedMem), Float2Units((float)mem.totalMem), perc);
    auto barStr = fmt::format("{:^{}}", barText, w.Size().x - 8);

    w.WriteXY(0, 2, "Mem [");
    ColorBar(w, barStr, perc, perc < 80 ? 11 : 12, 13);
    w.SetColor(6);
    w.Write("]");

    perc = mem.perc_swapUsed;
    barText = fmt::format("{}/{}(%{:.2f})", Float2Units((float)mem.swapUsed), Float2Units((float)mem.swapTotal), perc);
    barStr = fmt::format("{:^{}}", barText, w.Size().x - 8);

    w.WriteXY(0, 3, "Swp [");
    ColorBar(w, barStr, perc, perc < 80 ? 11 : 12, 13);
    w.SetColor(6);
    w.Write("]");


    w.Refresh();
}

void StatApp::ShowHelp()
{
    auto w = t.NewWindow(20, 20, 80, 35);
    w.Erase();
    w.FillBg();
    w.Border();
    w.Refresh();
    w.SetColor(A_UNDERLINE);
    w.SetColor(8);
    w.WriteXY(5, 2, "Apache access log realtime viewer");
    w.UnsetColor(A_UNDERLINE);
    w.WriteXY(5, 4, "Keyboard shortcuts");

    std::vector<std::pair<std::string, std::string>> optsList{
        {"d", "switch between url and source view"},
        {"s", "forward change sort column"},
        {"S", "backward change sort column"},
        {"SPACE", "refresh display"},
        {"r", "restart counters"},
        // {"o", "other stats"},
        {"h/?", "this help"},
        {"q", "quit"}};
    int pos = 6;
    for (const auto &opt : optsList)
    {
        w.SetColor(9);
        w.WriteXY(5, pos++, fmt::format("{:^7s}", opt.first));
        w.SetColor(8);
        w.Write(" - " + opt.second);
    }
    w.NoDelay(false);
    w.GetKey();
    w.NoDelay(true);
}
