#include "tools.h"

// std::string Float2Units(long long n)
// {
//     std::string str;
//     if (n < 1024ll*100)
//         str = std::to_string(n) + "B";
//     else if (n < 1024ll * 1024*100)
//         str = std::to_string(n / 1024) + "KB";
//     else if (n < 1024ll * 1024 * 1024*100)
//         str = std::to_string(n / (1024ll * 1024)) + "MB";
//     else
//         str = std::to_string(n / (1024ll * 1024 * 1024)) + "GB";
//     return str;
// }

std::string Float2Units(double n, int decimals, bool base10)
{
    std::string str;
    double mul = 1024;

    if (base10)
        mul = 1000;
    if (n < mul)
        return fmt::format("{:.{}f}B", n, decimals);
    else if (n < mul * mul)
        return fmt::format("{:.{}f}KB", n / mul, decimals);
    else if (n < mul * mul * mul)
        return fmt::format("{:.{}f}MB", n / (mul * mul), decimals);
    return fmt::format("{:.{}f}GB", n / (mul * mul * mul), decimals);
}
