#include "os_info.h"
#include <algorithm>
#include <ctype.h>
#include <filesystem>
#include <fstream>
#include <limits>
#include <set>
#include <unordered_map>

constexpr auto SSmax = std::numeric_limits<std::streamsize>::max();
namespace fs = std::filesystem;

OSInfo::OSInfo()
{
    pageSize = sysconf(_SC_PAGE_SIZE);
    if (pageSize <= 0)
        pageSize = 4096;
    cpuCount = (int)sysconf(_SC_NPROCESSORS_ONLN);
    Refresh();
}

void OSInfo::Refresh()
{
    if (updateTime + std::chrono::seconds(2) > std::chrono::steady_clock::now())
        return;
    updateTime = std::chrono::steady_clock::now();
    RefreshLoadAvg();
    RefreshMemInfo();
    RefreshProcessList();
}

void OSInfo::RefreshLoadAvg()
{
    std::ifstream loadavgFile("/proc/loadavg");

    if (loadavgFile.is_open())
    {
        loadavgFile >> cpuLoad.load1 >> cpuLoad.load5 >> cpuLoad.load15;
        loadavgFile.close();
    }
    else
    {
        cpuLoad.load1 = 0;
        cpuLoad.load5 = 0;
        cpuLoad.load15 = 0;
    };
}

void OSInfo::RefreshMemInfo()
{
    memInfo.availableMem = 0;
    std::ifstream meminfo("/proc/meminfo");
    if (meminfo.good())
    {
        while (!meminfo.eof())
        {
            std::string label;
            meminfo >> label;

            if (label == "MemTotal:")
            {
                meminfo >> memInfo.totalMem;
                memInfo.totalMem *= 1024;
            }
            else if (label == "MemFree:")
            {
                meminfo >> memInfo.freeMem;
                memInfo.freeMem *= 1024;
            }
            else if (label == "Cached:")
            {
                meminfo >> memInfo.cacheMem;
                memInfo.cacheMem *= 1024;
            }
            else if (label == "SwapTotal:")
            {
                meminfo >> memInfo.swapTotal;
                memInfo.swapTotal *= 1024;
            }
            else if (label == "SwapFree:")
            {
                meminfo >> memInfo.swapFree;
                memInfo.swapFree *= 1024;
            }
            else if (label == "MemAvailable:")
            {
                meminfo >> memInfo.availableMem;
                memInfo.availableMem *= 1024;
            }
            else
                meminfo.ignore(SSmax, '\n');
        };
    };
    if (memInfo.availableMem == 0)
        memInfo.availableMem = memInfo.freeMem + memInfo.cacheMem;
    memInfo.usedMem = memInfo.totalMem - memInfo.availableMem;
    memInfo.swapUsed = memInfo.swapTotal - memInfo.swapFree;

    memInfo.perc_freeMem = (float)memInfo.freeMem / (float)memInfo.totalMem * 100.0f;
    memInfo.perc_usedMem = (float)memInfo.usedMem / (float)memInfo.totalMem * 100.0f;
    memInfo.perc_availableMem = (float)memInfo.availableMem / (float)memInfo.totalMem * 100.0f;
    memInfo.perc_cacheMem = (float)memInfo.cacheMem / (float)memInfo.totalMem * 100.0f;
    memInfo.perc_swapUsed = (float)memInfo.swapUsed / (float)memInfo.swapTotal * 100.0f;
    memInfo.perc_swapFree = (float)memInfo.swapFree / (float)memInfo.swapTotal * 100.0f;
}

void OSInfo::RefreshProcessList()
{
    procList.clear();
    std::set<size_t> kernelPids = {2};
    for (const auto &d : fs::directory_iterator("/proc/"))
    {
        const std::string pid_str = d.path().filename();
        if (!fs::is_directory(d) || !std::isdigit(pid_str[0]))
            continue;
        ProcInfo proc;
        proc.pid = std::stol(pid_str);

        std::ifstream fd;

        fd.open(d.path() / "status");
        if (fd.good())
        {
            std::string line;
            int itemsCount = 2;
            while (!fd.eof() && fd.good() && itemsCount > 0)
            {
                fd >> line;
                if (line == "PPid:")
                {
                    fd >> proc.ppid;
                    itemsCount--;
                }
                else if (line == "Name:")
                {
                    fd >> proc.command;
                    itemsCount--;
                }
                else
                    fd.ignore(SSmax, '\n');
            }
        }
        fd.close();
        // check is kernel process
        if (kernelPids.contains(proc.ppid))
        {
            kernelPids.insert(proc.pid);
            continue;
        }
        procList.push_back(proc);
    }
    std::unordered_map<std::string, int> counter;
    for (auto &i : procList)
        ++counter[i.command];
    orderedProcList.clear();
    orderedProcList.assign(counter.begin(),
                           counter.end());
    std::sort(orderedProcList.begin(), orderedProcList.end(), [](auto a, auto b)
              { return a.second > b.second; });
}

std::string MemInfo::values()
{
    std::stringstream ss;
    ss << "mem total: " << Float2Units((double)totalMem) << ' ';
    ss << "free: " << Float2Units((double)freeMem) << ' ';
    ss << "used: " << Float2Units((double)usedMem) << ' ';
    ss << "cache: " << Float2Units((double)cacheMem) << ' ';
    ss << "available: " << Float2Units((double)availableMem) << ' ';
    ss << "Swap Total: " << Float2Units((double)swapTotal) << ' ';
    ss << "Free: " << Float2Units((double)swapFree) << ' ';
    ss << "Used: " << Float2Units((double)swapUsed) << '\n';
    return ss.str();
}

// Returns percents values
std::string MemInfo::percents()
{
    return fmt::format("free: %{:>0.2f} used: %{:>0.2f} available: %{:>0.2f} cache: %{:>0.2f} swapUsed: %{:>0.2f} swapFree: %{:>0.2f}",
                       perc_freeMem, perc_usedMem, perc_availableMem, perc_cacheMem, perc_swapUsed, perc_swapFree);
}