#include "log_stats.h"
#include "config.h"
#include <chrono>
#include <iomanip>

template <typename ContainerT, typename PredicateT>
void erase_if(ContainerT &items, const PredicateT &predicate)
{
    for (auto it = items.begin(); it != items.end();)
    {
        if (predicate(*it))
            it = items.erase(it);
        else
            ++it;
    }
}

void ParseLogFile::CleanOldEntries()
{
    const time_t maxOld = time(NULL) - cfg::GetItemsKeepTime();
    for (auto it = urlList.begin(); it != urlList.end();)
    {
        if (it->GetTime() < maxOld)
        {
            auto normalPath = it->NormalizedPath();
            auto normalSource = it->NormalizedSource();

            counterPerSource[normalSource] -= *it;
            if (counterPerSource[normalSource].GetCount() == 0)
                counterPerSource.erase(normalSource);

            counterPerPath[normalPath] -= *it;
            if (counterPerPath[normalPath].GetCount() == 0)
                counterPerPath.erase(normalPath);
            it = urlList.erase(it);
        }
        else
        {
            ++it;
        }
    }
    timeCounters.TimeCleanup();
}

void ParseLogFile::Loop()
{
    // Loop for 100 milliseconds
    auto timeNow = std::chrono::steady_clock::now();
    while (std::chrono::steady_clock::now() - timeNow < std::chrono::milliseconds(300))
    {
        std::string line = logFile.ReadLine();
        if (line == "")
            break;
        auto lline = LogLine(line);
        if (!lline.Parse())
            return;
        linesParsed++;
        urlList.push_back(lline);
        auto normalPath = lline.NormalizedPath();
        auto normalSource = lline.NormalizedSource();

        if (counterPerSource.find(normalSource) != counterPerSource.end())
            counterPerSource[normalSource] += lline;
        else
            counterPerSource[normalSource] = lline;

        if (counterPerPath.find(normalPath) != counterPerPath.end())
            counterPerPath[normalPath] += lline;
        else
            counterPerPath[normalPath] = lline;
        auto timeOfLine = lline.GetTime();
        if (timeOfLine >= time(NULL) - 10 * 60)
            timeCounters[timeOfLine]++;
    };
    if (lastCleanUp + std::chrono::seconds(1) < std::chrono::steady_clock::now())
    {
        lastCleanUp = std::chrono::steady_clock::now();
        CleanOldEntries();
    }
}

const PathSummaryMap ParseLogFile::GetTopTenPath() const
{
    return counterPerPath;
}

const SourceSummaryMap ParseLogFile::GetTopTenSource() const
{
    return counterPerSource;
}

void ParseLogFile::RestartCounters()
{
    urlList.clear();
    counterPerPath.clear();
    counterPerSource.clear();
    timeCounters.clear();
}

void TimeCounters::TimeCleanup()
{
    erase_if(countersList, [](const auto &a)
             { return a.first < time(NULL) - 15 * 60; });
}

const LoadCounters TimeCounters::GetLoadCounters() const
{
    LoadCounters c;
    time_t now = time(NULL);
    c.minTime = now;
    for (auto &item : countersList)
    {
        c.minTime = std::min(c.minTime, item.first);
        if (now - item.first < 15 * 60)
            c.counter15 += item.second;
        if (now - item.first < 5 * 60)
            c.counter5 += item.second;
        if (now - item.first < 1 * 60)
            c.counter1 += item.second;
    }
    c.minTime = time(NULL) - c.minTime;
    c.rate1 = (float)c.counter1 / (float)std::min(c.minTime, (time_t)(1 * 60));
    c.rate5 = (float)c.counter5 / (float)std::min(c.minTime, (time_t)(5 * 60));
    c.rate15 = (float)c.counter15 / (float)std::min(c.minTime, (time_t)(15 * 60));
    return c;
}
