* To build:
 ```
 cmake -S [SRC_DIR] -B [BUILD_DIR] -DCMAKE_BUILD_TYPE:STRING=Release &&  cmake --build [BUILD_DIR]
```
* bin:
   
   [BUILD_DIR]/src/access_log_stats

* Apache log format:
```
 LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %T/%D" combined
 ```